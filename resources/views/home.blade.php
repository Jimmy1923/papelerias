@extends('layouts/app')
@section('title', 'Centro de trabajo')
@section('content')
<style type="text/css">
 .table {
    width: 100%;
    text-align: center;
} 
</style>
<div class="intro-y flex flex-col sm:flex-row items-center mt-8">
    <h2 class="text-lg font-medium mr-auto">
        Centro de trabajo
    </h2>
    <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
        <a class="button text-white bg-theme-1 shadow-md mr-2" href="javascript:;" data-toggle="modal" data-target="#crear_empresa" >Crear nuevo centro de trabajo</a>
    </div>
</div><br>
<!-- BEGIN: Datatable -->
<div class="intro-y datatable-wrapper overflow-x-auto">
    <table class="table table-report table-report--bordered display datatable w-full">
        <thead>
            <tr>
                <th class="border-b-2 whitespace-no-wrap">Id</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Nombre</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Telefono</th>
                <th class="border-b-2 text-center whitespace-no-wrap">RFC</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Direccion</th>
                <th class="border-b-2 text-center whitespace-no-wrap">Opciones</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
<!-- END: Datatable -->
@endsection
